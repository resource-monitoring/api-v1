import { Module } from '@nestjs/common';
import { ElasticsearchModule } from '@nestjs/elasticsearch';

import { PageViewController } from './page-view/page-view.controller';
import { PageViewService } from './page-view/page-view.service';


@Module({
  imports: [
    ElasticsearchModule.register({
      node: process.env.ELASTICSEARCH_URL,
    }),
  ],
  controllers: [PageViewController],
  providers: [PageViewService],
})
export class AppModule {}
