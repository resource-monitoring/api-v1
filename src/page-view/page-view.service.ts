import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';

@Injectable()
export class PageViewService {
    private ES_INDEX: string = 'page-view';

    constructor(
        private readonly elasticsearchService: ElasticsearchService
    ) {}

    public async count(): Promise<string> {
        const { body } = await this.elasticsearchService.count({
            index: this.ES_INDEX,
            body: {
                query: { match_all: {} }
            }
        })

        return body.count
    }
}
