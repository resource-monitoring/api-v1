import { Controller, Get } from '@nestjs/common';

import { PageViewService } from './page-view.service';

@Controller('page-view')
export class PageViewController {
    constructor(private pageViewService: PageViewService) {}

    @Get('count')
    save(): Promise<string> {
        return this.pageViewService.count();
    }
}
